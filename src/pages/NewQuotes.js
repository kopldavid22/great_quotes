import { useHistory } from "react-router-dom";
import QuoteForm from "../components/quotes/QuoteForm";
const NewQuotes = () => {
  const history = useHistory();
  const addQuoteHandler = (quoteData) => {
    console.log(quoteData);
    history.push("/quotes"); //It could direct me into my history route, so it will after send data reroute into domain.xy/quotes
  };
  return <QuoteForm onAddQuote={addQuoteHandler} />;
};

export default NewQuotes;
