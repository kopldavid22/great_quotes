import classes from './CommentItem.module.css';
import { useParams } from 'react-router-dom';
const CommentItem = (props) => {
  const params = useParams();
  return (
    <li className={classes.item}>
      <p>{props.text}</p>
      <p>{params.quoteId}</p>
    </li>
  );
};

export default CommentItem;
