import { Route, Switch, Redirect } from "react-router-dom";
import AllQuotes from "./pages/AllQuotes";
import NewQuote from "./pages/NewQuotes";
import QuoteDetail from "./pages/QuoteDetail";
// import CommentItem from './components/comments/CommentItem';
import Layout from "./components/layout/Layout";
import NotFound from "./pages/NotFound";

function App() {
  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          {/*From nowhere /  ... it will redirect into /quotes💪 */}
          <Redirect to="/quotes" />
        </Route>
        {/*When Switch find a match it ll stop*/}
        <Route path="/quotes" exact>
          {/*It ll skip when here will be domain/quotes/something */}
          <AllQuotes />
        </Route>
        <Route path="/quotes/:quoteId">
          {/*It s params :quoteId*/}
          <QuoteDetail />
        </Route>
        {/* <Route path="/quotes/:quoteId/comment">
        <CommentItem />
      </Route> */}
        <Route path="/new-quote">
          <NewQuote />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
        {/*Not found page! */}
      </Switch>
    </Layout>
  );
}

export default App;
